const low = require("lowdb");
const FileSync = require("lowdb/adapters/FileSync");

const adapter = new FileSync("db.json");
const db = low(adapter);

exports.handler = function(event, context, callback) {
  const posts = db
    .get("posts")
    .find({ id: 1 })
    .value();

  // your server-side functionality
  callback(null, {
    statusCode: 200,
    body: JSON.stringify({
      message: posts
    })
  });
};
